# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

description     "Start servod based on config files"
author          "chromium-os-dev@chromium.org"

start on started system-services

env CONFIG_DIR="/var/lib/servod"

pre-start script
  # Create the config dir if it doesn't exist.
  if [ ! -d $CONFIG_DIR ]; then
    mkdir -p $CONFIG_DIR
  fi
end script

script
  # Config files are in the format config_$PORT, pull out the port and
  # pass it in to the upstart job.
  for PORT in $(ls ${CONFIG_DIR}/config_* | cut -d _ -f 2); do
    start servod PORT=$PORT
    # Stagger servod startup so they have time to reset/update servo v4
    # if necessary and avoid potentially browning out the hubs.
    sleep 8
  done
end script
