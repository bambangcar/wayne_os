From 7792ec3952cde78e6a465330dc7c82969c59c63d Mon Sep 17 00:00:00 2001
From: Chris Morin <chris.morin2@gmail.com>
Date: Wed, 20 Mar 2019 01:34:23 -0700
Subject: [PATCH 1/2] journal-file: handle SIGBUS on offlining thread

The thread launched in journal_file_set_offline() accesses a memory
mapped file, so it needs to handle SIGBUS. Leave SIGBUS unblocked on the
offlining thread so that it uses the same handler as the main thread.

The result of triggering SIGBUS in a thread where it's blocked is
undefined in Linux. The tested implementations were observed to cause
the default handler to run, taking down the whole journald process.

We can leave SIGBUS unblocked in multiple threads since it's handler is
thread-safe. If SIGBUS is sent to the journald process asynchronously
(i.e. with kill, sigqueue, or raise), either thread handling it will
result in the same behavior: it will install the default handler and
reraise the signal, killing the process.

Fixes: #12042
---
 src/journal/journal-file.c | 6 ++++--
 1 file changed, 4 insertions(+), 2 deletions(-)

diff --git a/src/journal/journal-file.c b/src/journal/journal-file.c
index dc98c38c72..c332fff092 100644
--- a/src/journal/journal-file.c
+++ b/src/journal/journal-file.c
@@ -235,8 +235,10 @@ int journal_file_set_offline(JournalFile *f, bool wait) {
                 sigset_t ss, saved_ss;
                 int k;
 
-                if (sigfillset(&ss) < 0)
-                        return -errno;
+                assert_se(sigfillset(&ss) >= 0);
+                /* Don't block SIGBUS since the offlining thread accesses a memory mapped file.
+                 * Asynchronous SIGBUS signals can safely be handled by either thread. */
+                assert_se(sigdelset(&ss, SIGBUS) >= 0);
 
                 r = pthread_sigmask(SIG_BLOCK, &ss, &saved_ss);
                 if (r > 0)
-- 
2.21.0.392.gf8f6787159e-goog

