#!/usr/bin/python
# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Entry point for running Whining Matrix dashboard on wsgi.

Runs on top of an existing Apache server.
"""
import argparse, bottle, logging, os, socket, sys

base_dir = os.path.dirname(__file__)
os.chdir(base_dir)
sys.path.insert(0, base_dir)

# Point the db properly.
from src import settings, urls

# Establish bases for templates and static.
templates_dir = os.path.join(base_dir, 'templates')
static_root = os.path.join(base_dir, 'static')
bottle.TEMPLATE_PATH.append(templates_dir)

application = bottle.default_app()

# TODO (drinkcat): Move this to config file
is_down = False
run_debug = False

application.catchall = False

# Map routes onto the app object.
# If the --down param was set, route everything to static/down.html
if is_down:
    @app.error(404)
    def route_system_down_message(code):
        return bottle.static_file('down.html', static_root)
else:
    urls.add_routes(application, static_root)

settings.settings = settings.Settings(os.path.join(base_dir, 'config.ini'))

logging_level = logging.INFO
if run_debug:
    logging_level = logging.DEBUG
    print 'Serving with "debug" enabled.'
logging.basicConfig(level=logging_level)
