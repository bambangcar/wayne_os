// Copyright 2018 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package cryptohome contains local Tast tests that exercise the cryptohomed daemon.
//
// See https://chromium.googlesource.com/chromiumos/platform2/+/master/cryptohome for more details.
package cryptohome
