syntax = 'proto3';
package chromite.api;

option go_package = "go.chromium.org/chromiumos/infra/proto/go/chromite/api";

import "chromite/api/build_api.proto";
import "chromite/api/sysroot.proto";
import "chromiumos/common.proto";

// An artifact is a file generated during or after a build.
message Artifact {
  // Absolute path to the artifact file.
  string path = 1;
}

// Request describing where build artifact bundles for a given build target
// should be dumped.
message BundleRequest {
  // The build target to bundle artifacts for.
  // Deprecated. Use sysroot instead.
  chromiumos.BuildTarget build_target = 1;

  // Absolute path to the directory in which artifacts should be dropped.
  string output_dir = 2;

  // The chroot where the sysroot lives.
  chromiumos.Chroot chroot = 3;
  // The sysroot where the files live.
  chromite.api.Sysroot sysroot = 4;
}

// Response describing which bundles were dumped to the given output directory.
message BundleResponse {
  // The artifacts that added to the output directory.
  repeated Artifact artifacts = 1;
}

// Request describing where to find build artifacts that are taken from a
// sysroot.
message BundleVmFilesRequest {
  // The chroot where the sysroot lives.
  chromiumos.Chroot chroot = 1;
  // The sysroot where the files live.
  chromite.api.Sysroot sysroot = 2;
  // Test results directory relative to the sysroot.
  string test_results_dir = 3;

  // Absolute path to the directory in which artifacts should be dropped.
  string output_dir = 4;
}

// Request describing where to find Chrome orderfile artifacts
message BundleChromeOrderfileRequest {
  // The chroot where the orderfile lives.
  chromiumos.Chroot chroot = 1;
  // The build_target of the builder.
  chromiumos.BuildTarget build_target = 2;
  // The Chrome version that builder uses
  string chrome_version = 3;
  // Absolute path to the directory in which artifacts should be dropped.
  string output_dir = 4;
}

// Fetch the pinned guest image uris.
message PinnedGuestImageUriRequest {
  // The chroot where the sysroot lives.
  chromiumos.Chroot chroot = 1;
  // The sysroot where the files live.
  chromite.api.Sysroot sysroot = 2;
}

// Response from fetching pinned guest image uris.
message PinnedGuestImageUriResponse {
  message PinnedGuestImage {
    string filename = 1;
    string uri = 2;
  }

  // The pinned guest images.
  repeated PinnedGuestImage pinned_images = 1;
}

// Service for bundling build artifacts and dumping them somewhere on disk.
service ArtifactsService {
  option (service_options) = {
    module: "artifacts",
    service_chroot_assert: OUTSIDE,
  };

  // Create a tar archive with all files needed for Autotest HW testing.
  rpc BundleAutotestFiles(BundleRequest) returns (BundleResponse);

  // Create a tar archive with ebuild logs.
  rpc BundleEbuildLogs(BundleRequest) returns (BundleResponse);

  // Create a tar archive with unsigned firmware images.
  rpc BundleFirmware(BundleRequest) returns (BundleResponse);

  // Generate zip containing all built images for the target.
  rpc BundleImageZip(BundleRequest) returns (BundleResponse);

  // Create Chrome orderfile builder artifacts.
  rpc BundleOrderfileGenerationArtifacts(BundleChromeOrderfileRequest)
    returns (BundleResponse);

  // Create a tar archive with all guest images test bundles.
  rpc BundlePinnedGuestImages(BundleRequest) returns (BundleResponse);

  // Create the simple chrome artifacts.
  rpc BundleSimpleChromeArtifacts(BundleRequest) returns (BundleResponse);

  // Create a tar archive with all files needed for Tast HW testing.
  rpc BundleTastFiles(BundleRequest) returns (BundleResponse);

  // Generate minimal update payloads to be used in HW testing.
  rpc BundleTestUpdatePayloads(BundleRequest) returns (BundleResponse);

  // Create a tar archive with VM memory and disk images.
  rpc BundleVmFiles(BundleVmFilesRequest) returns (BundleResponse);

  // Fetch the pinned guest image locations.
  rpc FetchPinnedGuestImageUris(PinnedGuestImageUriRequest)
    returns (PinnedGuestImageUriResponse);
}
